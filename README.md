# 笋丁轻论坛

## 程序介绍
- 轻论坛系统，目前只完善了基本功能，蜗速迭代中...

- 前端Vue3 + Vite + Pinia    后端Golang + Gin

- 演示站：[http://sd.cnm.sb](http://sd.cnm.sb) (PC端UI未适配，请使用移动端预览)


## 写在前面

-  程序所涉及的任何内容，包括但不限于文字，图片，图标，代码等等任何侵犯您权利的内容，请联系我们妥善处理！

-  企鹅交流群：826866498，大家加入群聊一起玩耍

    -  不想扫码？[点我加入](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=93nBg5lf-u3pR4bfSmMp4iiD4BjaxzSq&authKey=xuN0agwvBAckmy56wDcXr4N44HdTE4Qsdb5mOn8FDJzwBcpeaKf8LuedgQdBqd2X&noverify=0&group_code=826866498)
    -  <img src="other-images/826866498.jpg" alt="Q群826866498" width="300">


## 事先准备
- 一台服务器，已安装宝塔面板，运行环境如下
    - 操作系统：Linux Centos7.6
    - Web服务器：Nginx 1.22.1
    - 数据库：Mysql 5.7.40
    - 数据库：Redis 7.0.11
    - 其他版本环境请自行测试

- 一个域名，域名先解析二条记录，方便后面使用
    - 前端页面：用户前台 demo.sunding.cn（子目录admin为管理员后台 demo.sunding.cn/admin）
    - 后端API：例如 api.sunding.cn

- 程序源码下载方式：
    - 一：QQ群文件，内测版.zip
    - 二：直链：[https://cnm.sb/code/code_alpha_20240314.zip](http://101.32.37.124:8888/down/omNHSobTm9la.zip)


## 教程开始

### 🦀添加Mysql数据库
![添加Mysql数据库](deployment-tutorial/add-mysql.png)


### 🥝查看Redis数据库密码

![查看Redis数据库密码](deployment-tutorial/redis.png)


### 🍓部署前端用户前台和管理员后台

![添加前端站点](deployment-tutorial/add-website.png)
![上传前端源码](deployment-tutorial/upload-front-code.png)


### 🍍部署后端API

    - 注意：端口为6621
    - 注意：端口为6621
    - 注意：端口为6621
    - 注意：端口为6621
    - 注意：端口为6621

![上传后端代码](deployment-tutorial/upload-backend-code.png)
![部署Go项目](deployment-tutorial/add-go.png)


### 🍊配置nginx规则

    # nginx反向代理代码
    location / {
        index index.html;
        try_files $uri $uri/ /index.html;
    }
    
    location /admin/ {
        index index.html;
        try_files $uri $uri/ /admin/index.html;
    }
    
    location ^~ /sd {
    		proxy_pass http://请更换为你的Go后端绑定的域名;
    		proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
![配置nginx规则](deployment-tutorial/nginx.png)


### 🥭执行安装
![初始化Mysql数据库](deployment-tutorial/install-mysql.png)
![连接Redis数据库](deployment-tutorial/install-redis.png)


### 🍋安装成功
| 默认生成 | 用户名   | 密码     | 备注           |
|------|-------|--------|--------------|
| 管理员  | admin | 123456 | 登录成功之后请务必修改密码 |


### 🍉自此程序部署结束，感谢您的支持！
    搭建过程中有疑问请进入Q群咨询群友，有好的想法或建议意见也可告诉我们！
    当前为内测阶段，域名免费授权（永久免费更新，内测用户后期也不会收费），快来白嫖吧！

版本迭代

    - 后端：直接替换后端的main文件，storage文件夹和config.yaml文件不用管，替换main之后，前往go项目重启即可

    - 前端：前端包括用户前台和管理员后台，直接删除index.html文件，assets文件夹和admin目录，重新上传即可，nginx规则已经配置无需更改

## 🍈特别鸣谢

本项目使用了以下开源组件，并在此表示感谢：

- [Vue 3](https://github.com/vuejs/vue-next)
- [Vite](https://github.com/vitejs/vite)
- [Element Plus](https://github.com/element-plus/element-plus)
- [Pinia](https://github.com/vuejs/pinia)
- [Redis](https://github.com/redis/redis)
- [MySQL](https://github.com/mysql/mysql-server)

- [Golang](https://github.com/golang/go)
- [Gin](https://github.com/gin-gonic/gin)
- 等等，待完善

感谢以上组件的作者为我们提供优秀的开源软件！

## 🔑 License
MIT


## 💰赞助
- 赞助二维码（赞助之后请联系我们，将您添加到名单中）

    <img src="other-images/%E6%89%93%E8%B5%8F.png" alt="Q群826866498" width="375">

- 名单
    
    | ID | 昵称 | 金额 | 留言 |
    |----|----|----|----|
    | 1  | 超耐磨  | ¥800  | https://cnm.sb  |
    | 2  | 执笔| ¥15  | http://zbiwl.com  |
    | 3  | ChowSom| ¥9.99  | https://bbs.6al.net  |
    | 4  | 大帅比| ¥8.88  | http://bd.al  |
    | 5  | 橙七| ¥5  | 做大做强  |


    

Copyright (c) 2024 www.sunding.cn
